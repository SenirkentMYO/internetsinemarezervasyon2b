﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sinemarezervasyontakipsistemi
{
    public class BiletKayit
    {
        public int ID { get; set; }
        public string Ad { get; set; }
        public string SoyAd { get; set; }
        public int KoltukNo { get; set; }
        public string FilmSaati { get; set; }
        public int SalonNo { get; set; }
        public string Fiyatı { get; set; }
        public string BiletNo { get; set; }
    }
}